/**/

#include "dgram.h"

int cht_setnonblock(int fd, int isnonblock) {
  int r;

  r= fcntl(fd,F_GETFL); 
  if (r==-1) return -1;
  r= fcntl(fd,F_SETFL, isnonblock ? r|O_NONBLOCK : r&~O_NONBLOCK);
  if (r==-1) return -1;
  return 0;
}

