/*
 */
/*
 * dgram-socket create <local>                        => <sockid>
 * dgram-socket close <sockid>
 * dgram-socket transmit <sockid> <data> <remote>
 * dgram-socket on-receive <sockid> [<script>]
 *    calls, effectively,  eval <script> [list <data> <remote-addr> <socket>]
 *    if script not supplied, cancel
 */

#include "dgram.h"

typedef struct DgramSocket {
  int ix; /* first ! */
  int fd;
  Tcl_Interp *ip;
  ScriptToInvoke script;
  void *addr_buf, *msg_buf;
  int addr_buflen, msg_buflen;
} DgramSocket;

int cht_do_dgramsocket_create(ClientData cd, Tcl_Interp *ip,
			   SockAddr_Value local, void **sock_r) {
  int fd, al, r;
  DgramSocket *sock;
  const struct sockaddr *sa;

  sa= cht_sockaddr_addr(&local);
  al= cht_sockaddr_len(&local);

  fd= socket(sa->sa_family, SOCK_DGRAM, 0);
  if (fd<0) return cht_posixerr(ip,errno,"socket");
  r= bind(fd, sa, al);  if (r) return cht_newfdposixerr(ip,fd,"bind");
  r= cht_setnonblock(fd, 1);  if (r) return cht_newfdposixerr(ip,fd,"setnonblock");

  sock= TALLOC(sizeof(DgramSocket));
  sock->ix= -1;
  sock->fd= fd;
  sock->addr_buflen= al+1;
  sock->addr_buf= TALLOC(sock->addr_buflen);
  sock->msg_buflen= 0;
  sock->msg_buf= 0;
  cht_scriptinv_init(&sock->script);

  *sock_r= sock;
  return TCL_OK;
}

int cht_do_dgramsocket_transmit(ClientData cd, Tcl_Interp *ip,
			     void *sock_v, HBytes_Value data,
			     SockAddr_Value remote) {
  DgramSocket *sock= sock_v;
  int l, r;

  r= sendto(sock->fd,
	    cht_hb_data(&data), l=cht_hb_len(&data),
	    0,
	    cht_sockaddr_addr(&remote), cht_sockaddr_len(&remote));
  if (r==-1) return cht_posixerr(ip,errno,"sendto");
  else if (r!=l) return cht_staticerr(ip,"sendto gave wrong answer",0);
  return TCL_OK;
}

static void cancel(DgramSocket *sock) {
  if (sock->script.script) {
    cht_scriptinv_cancel(&sock->script);
    Tcl_DeleteFileHandler(sock->fd);
  }
}

static void recv_call(ClientData sock_cd, int mask) {
  DgramSocket *sock= (void*)sock_cd;
  Tcl_Interp *ip= sock->script.ipq;
  int sz, rc, peek;
  HBytes_Value message_val;
  SockAddr_Value peer_val;
  Tcl_Obj *args[3];
  struct msghdr mh;
  struct iovec iov;

  cht_hb_empty(&message_val);
  cht_sockaddr_clear(&peer_val);

  mh.msg_iov= &iov;
  mh.msg_iovlen= 1;
  mh.msg_control= 0;
  mh.msg_controllen= 0;
  mh.msg_flags= 0;

  peek= MSG_PEEK;
  
  for (;;) {
    mh.msg_name= sock->addr_buf;
    mh.msg_namelen= sock->addr_buflen;

    iov.iov_base= sock->msg_buf;
    iov.iov_len= sock->msg_buflen;

    sz= recvmsg(sock->fd, &mh, peek);
    if (sz==-1) {
      if (errno == EAGAIN || errno == EWOULDBLOCK) rc=0;
      else rc= cht_posixerr(ip,errno,"recvmsg");
      goto x_rc;
    }

    assert(mh.msg_namelen < sock->addr_buflen);

    if (!(mh.msg_flags & MSG_TRUNC)) {
      if (!peek) break;
      peek= 0;
      continue;
    }

    TFREE(sock->msg_buf);
    assert(sock->msg_buflen < INT_MAX/4);
    sock->msg_buflen *= 2;
    sock->msg_buflen += 100;
    sock->msg_buf= TALLOC(sock->msg_buflen);
  }

  cht_hb_array(&message_val, iov.iov_base, sz);
  cht_sockaddr_create(&peer_val, mh.msg_name, mh.msg_namelen);

  args[0]= cht_ret_hb(ip, message_val);  cht_hb_empty(&message_val);
  args[1]= cht_ret_sockaddr(ip, peer_val);  cht_sockaddr_clear(&peer_val);
  args[2]= cht_ret_iddata(ip, sock, &cht_dgram_socks);
  cht_scriptinv_invoke(&sock->script,3,args);

  rc= 0;
  
x_rc:
  if (rc)
    Tcl_BackgroundError(ip);
}

int cht_do_dgramsocket_sockname(ClientData cd, Tcl_Interp *ip,
				void *sock_v, SockAddr_Value *result) {
  DgramSocket *sock= sock_v;
  int r;

  socklen_t salen = sock->addr_buflen;
  r= getsockname(sock->fd, sock->addr_buf, &salen);
  if (r) return cht_posixerr(ip,errno,"getsockname");
  cht_sockaddr_create(result, sock->addr_buf, salen);
  return TCL_OK;
}

int cht_do_dgramsocket_on_receive(ClientData cd, Tcl_Interp *ip,
			       void *sock_v, Tcl_Obj *newscript) {
  DgramSocket *sock= sock_v;
  int rc;
  
  cancel(sock);
  
  if (newscript) {
    rc= cht_scriptinv_set(&sock->script, ip, newscript, 0);
    if (rc) return rc;
  }
  
  Tcl_CreateFileHandler(sock->fd, TCL_READABLE, recv_call, sock);
  return TCL_OK;
}

static void destroy(DgramSocket *sock) {
  cancel(sock);
  close(sock->fd); /* nothing useful to be done with errors */
  TFREE(sock->addr_buf);
  TFREE(sock->msg_buf);
  TFREE(sock);
}

static void destroy_idtabcb(Tcl_Interp *ip, void *sock_v) {
  destroy(sock_v);
}

int cht_do_dgramsocket_close(ClientData cd, Tcl_Interp *ip, void *sock_v) {
  cht_tabledataid_disposing(ip,sock_v,&cht_dgram_socks);
  destroy(sock_v);
  return TCL_OK;
}

const IdDataSpec cht_dgram_socks= {
  "dgramsock", "dgramsock-table", destroy_idtabcb
};
