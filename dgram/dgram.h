/* dgram - Tcl extension for udp datagrams
 * Copyright 2006-2012 Ian Jackson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DGRAM_H
#define DGRAM_H

#include "hbytes.h"

/* from sockaddr.c */

typedef struct {
  Byte *begin, *end;
} SockAddr_Value;

extern Tcl_ObjType sockaddr_type;

void cht_sockaddr_clear(SockAddr_Value*);
void cht_sockaddr_create(SockAddr_Value*, const struct sockaddr *addr, int al);
int cht_sockaddr_len(const SockAddr_Value*);
const struct sockaddr *cht_sockaddr_addr(const SockAddr_Value*);
void cht_sockaddr_free(const SockAddr_Value*);

/* from dgram.c */

extern const IdDataSpec cht_dgram_socks;

/* from misc.c */

int cht_setnonblock(int fd, int isnonblock);

#include "dgram+tcmdif.h"

#endif /*DGRAM_H*/
