/*
 */
/*
 * tuntap-socket-raw create [<ifname>] => <sockid>
 * tuntap-socket-raw ifname <sockid> => <ifname>
 * tuntap-socket-raw close <sockid>
 * tuntap-socket-raw receive <sockid> <data>
 * tuntap-socket-raw on-transmit <sockid> <mtu> [<script>]
 *    calls, effectively,  eval <script> [list <data> <socket>]
 *    if script not supplied, cancel
 */

#include "chiark_tcl_tuntap.h"

typedef struct TunSocket {
  int ix, fd, script_llength;
  Tcl_Interp *ip;
  ScriptToInvoke script;
  int mtu;
  unsigned char *msg_buf;
  char *ifname;
} TuntapSocket;

int cht_do_tuntapsocket_create_tun(ClientData cd, Tcl_Interp *ip,
				const char *ifname, void **sock_r) {
  int fd, r;
  struct ifreq ifr;
  TuntapSocket *sock;

  memset(&ifr,0,sizeof(ifr));
  ifr.ifr_flags= IFF_TUN | IFF_NO_PI;
  
  if (ifname) {
    if (strlen(ifname) > IFNAMSIZ-1) return
      cht_staticerr(ip,"tun interface name too long","TUNTAP IFNAME LENGTH");
    strcpy(ifr.ifr_name, ifname);
  }

  fd= open("/dev/net/tun", O_RDWR);
  if (fd<0) return cht_posixerr(ip,errno,"open /dev/net/tun");

  r= cht_setnonblock(fd,1);
  if (r) return cht_posixerr(ip,errno,"setnonblock tun");
  
  r= ioctl(fd, TUNSETIFF, (void*)&ifr);
  if (r) return cht_newfdposixerr(ip,fd,"ioctl TUNSETIFF");

  sock= TALLOC(sizeof(TuntapSocket));
  sock->ix= -1;
  sock->fd= fd;
  sock->mtu= 0;
  sock->msg_buf= 0;
  sock->ifname= TALLOC(strlen(ifr.ifr_name)+1);
  strcpy(sock->ifname, ifr.ifr_name);
  cht_scriptinv_init(&sock->script);

  *sock_r= sock;
  return TCL_OK;
}

int cht_do_tuntapsocket_receive(ClientData cd, Tcl_Interp *ip,
				 void *sock_v, HBytes_Value data) {
  TuntapSocket *sock= sock_v;
  int l, r;

  r= write(sock->fd,
	   cht_hb_data(&data), l=cht_hb_len(&data));
  if (r==-1) return cht_posixerr(ip,errno,"write tuntap");
  else if (r!=l) return cht_staticerr(ip,"write tuntap gave wrong answer",0);
  return TCL_OK;
}

int cht_do_tuntapsocket_ifname(ClientData cd, Tcl_Interp *ip,
				void *sock_v, const char **result) {
  TuntapSocket *sock= sock_v;
  *result= sock->ifname;
  return TCL_OK;
}

static void cancel(TuntapSocket *sock) {
  if (sock->script.script) {
    cht_scriptinv_cancel(&sock->script);
    Tcl_DeleteFileHandler(sock->fd);
    TFREE(sock->msg_buf);
    sock->msg_buf= 0;
  }
}

static void read_call(ClientData sock_cd, int mask) {
  TuntapSocket *sock= (void*)sock_cd;
  Tcl_Interp *ip= sock->ip;
  int rc;
  ssize_t sz;
  HBytes_Value message_val;
  Tcl_Obj *args[2];

  for (;;) {
    sz= read(sock->fd, sock->msg_buf, sock->mtu);
    if (sz == -1) {
      if (errno == EAGAIN || errno == EWOULDBLOCK) rc=0;
      else rc= cht_posixerr(ip,errno,"read tuntap");
      goto x_rc;
    }

    assert(sz <= sock->mtu);

    cht_hb_array(&message_val, sock->msg_buf, sz);
    args[0]= cht_ret_hb(ip, message_val);  cht_hb_empty(&message_val);
    args[1]= cht_ret_iddata(ip, sock, &cht_tuntap_socks);
    cht_scriptinv_invoke(&sock->script, 2, args);
  }

x_rc:
  if (rc) Tcl_BackgroundError(ip);
}

int cht_do_tuntapsocket_on_transmit(ClientData cd, Tcl_Interp *ip,
				     void *sock_v,
				     long mtu, Tcl_Obj *newscript) {
  TuntapSocket *sock= sock_v;
  int rc;

  if (mtu > 65536)
    return cht_staticerr(ip,"tuntap mtu >2^16","TUNTAP MTU OVERRUN");

  cancel(sock);
  
  if (newscript) {
    rc= cht_scriptinv_set(&sock->script,ip,newscript,0);
    if (rc) return rc;
    
    sock->mtu= mtu;
    sock->msg_buf= TALLOC(mtu);
    Tcl_CreateFileHandler(sock->fd, TCL_READABLE, read_call, sock);
  }
  return TCL_OK;
}

static void destroy(void *sock_v) {
  TuntapSocket *sock= sock_v;
  cancel(sock);
  close(sock->fd); /* nothing useful to be done with errors */
  TFREE(sock->msg_buf);
  TFREE(sock);
}

static void destroy_idtabcb(Tcl_Interp *ip, void *sock_v) {
  destroy(sock_v);
}

int cht_do_tuntapsocket_close(ClientData cd, Tcl_Interp *ip, void *sock) {
  cht_tabledataid_disposing(ip,sock,&cht_tuntap_socks);
  destroy(sock);
  return TCL_OK;
}

const IdDataSpec cht_tuntap_socks= {
  "tuntap", "tuntap-table", destroy_idtabcb
};

CHT_INIT(tuntap, { }, CHTI_COMMANDS(cht_tuntaptoplevel_entries))
