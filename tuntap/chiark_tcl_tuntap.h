/*
 * tuntap - Tcl bindings for tun/tap userspace network interfaces
 * Copyright 2006-2012 Ian Jackson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TUNTAPTCL_H
#define TUNTAPTCL_H

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include <net/if.h>
#include <linux/if_tun.h>

#include "hbytes.h"
#include "dgram.h"
#include "tuntap+tcmdif.h"

/* from tuntap.c */

extern const IdDataSpec cht_tuntap_socks;

#endif /*TUNTAPTCL_H*/
