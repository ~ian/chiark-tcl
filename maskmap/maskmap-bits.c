/*
 * maskmap - Tcl extension for address mask map data structures
 * Copyright 2006-2012 Ian Jackson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 */


int cht_do_hbytes_addr_map(ClientData cd, Tcl_Interp *ip,
		       const AddrMap_SubCommand *subcmd,
		       int objc, Tcl_Obj *const *objv) {
  return subcmd->func(0,ip,objc,objv);
}

xxxx
extern int Chiark_tcl_hbytes_Init(Tcl_Interp *ip); /*called by load(3tcl)*/
int Chiark_tcl_hbytes_Init(Tcl_Interp *ip) {
  static int initd;
  
  return cht_initextension(ip, cht_hbytestoplevel_entries, &initd,
			   &cht_addrmap_type,
			   (Tcl_ObjType*)0);
}
