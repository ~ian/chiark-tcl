
SUBDIRS=	base adns hbytes cdb crypto dgram

ifneq ($(wildcard /usr/include/linux/if_tun.h),)
SUBDIRS+=	tuntap
endif

ifneq ($(wildcard /usr/include/wiringPi.h /usr/include/arm-*/wiringPi.h),)
SUBDIRS+=	wiringpi
endif

default: all

clean all debian-substvars:
	set -e; for d in $(SUBDIRS); do $(MAKE) -C $$d $@; done

# To find undefined symbols when implementing, for example:
#
# liberator:chiark-tcl> LD_LIBRARY_PATH=:adns:base:cdb:crypto:dgram:hbytes:tuntap tclsh8.3
# % load chiark_tcl_tuntap-1.so
# couldn't load file "chiark_tcl_tuntap-1.so": tuntap/chiark_tcl_tuntap-1.so: undefined symbol: cht_tunsocket_entries
# % 
