/*
 * hbytes - hex-stringrep efficient byteblocks for Tcl
 * Copyright 2006-2012 Ian Jackson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 */


#include "chiark_tcl_hbytes.h"

static int strs1(Tcl_Interp *ip, int strc, Tcl_Obj *const *strv, int *l_r) {
  int rc, l, i, pl;

  l= 0;
  for (i=1; i<strc; i++) {
    rc= Tcl_ConvertToType(ip,strv[i],&cht_hbytes_type);
    if (rc) return rc;
    pl= cht_hb_len(OBJ_HBYTES(strv[i]));
    assert(l < INT_MAX/2 && pl < INT_MAX/2);
    l += pl;
  }
  *l_r= l;
  return TCL_OK;
}

static void strs2(Byte *dest, int strc, Tcl_Obj *const *strv) {
  int tl, i;
  
  for (i=1; i<strc; i++) {
    tl= cht_hb_len(OBJ_HBYTES(strv[i]));
    memcpy(dest, cht_hb_data(OBJ_HBYTES(strv[i])), tl);
    dest += tl;
  }
}

int cht_do_hbytes_prepend(ClientData cd, Tcl_Interp *ip,
		      HBytes_Var v, int strc, Tcl_Obj *const *strv) {
  int rc, el;
  Byte *dest;
  
  rc= strs1(ip,strc,strv,&el);  if (rc) return rc;
  dest= cht_hb_prepend(v.hb, el);
  strs2(dest, strc,strv);
  return TCL_OK;
}
  
int cht_do_hbytes_append(ClientData cd, Tcl_Interp *ip,
		     HBytes_Var v, int strc, Tcl_Obj *const *strv) {
  int rc, el;
  Byte *dest;

  rc= strs1(ip,strc,strv,&el);  if (rc) return rc;
  dest= cht_hb_append(v.hb, el);
  strs2(dest,  strc,strv);
  return TCL_OK;
}

int cht_do_hbytes_concat(ClientData cd, Tcl_Interp *ip,
		     int strc, Tcl_Obj *const *strv, HBytes_Value *result) {
  int rc, l;
  Byte *dest;
  
  rc= strs1(ip,strc,strv,&l);  if (rc) return rc;
  dest= cht_hb_arrayspace(result,l);
  strs2(dest, strc,strv);
  return TCL_OK;
}

static int underrun(Tcl_Interp *ip) {
  return cht_staticerr(ip,"data underrun","HBYTES LENGTH UNDERRUN");
}

int cht_do_hbytes_unprepend(ClientData cd, Tcl_Interp *ip,
			HBytes_Var v, int preflength, HBytes_Value *result) {
  const Byte *rdata= cht_hb_unprepend(v.hb, preflength);
  if (!rdata) return underrun(ip);
  cht_hb_array(result, rdata, preflength);
  return TCL_OK;
}

int cht_do_hbytes_unappend(ClientData cd, Tcl_Interp *ip,
		       HBytes_Var v, int suflength, HBytes_Value *result) {
  const Byte *rdata= cht_hb_unappend(v.hb, suflength);
  if (!rdata) return underrun(ip);
  cht_hb_array(result, rdata, suflength);
  return TCL_OK;
}

int cht_do_hbytes_chopto(ClientData cd, Tcl_Interp *ip,
		     HBytes_Var v, int newlength, HBytes_Value *result) {
  int suflength= cht_hb_len(v.hb) - newlength;
  return cht_do_hbytes_unappend(0,ip,v, suflength, result);
}
