/*
 * hbytes - hex-stringrep efficient byteblocks for Tcl
 * Copyright 2006-2012 Ian Jackson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 */


#include <errno.h>

#include "chiark_tcl_hbytes.h"

int cht_do_hbytes_rep_info(ClientData cd, Tcl_Interp *ip,
		       Tcl_Obj *obj, Tcl_Obj **result) {
  const char *tn;
  int nums[3], i, lnl;
  Tcl_Obj *objl[4];

  if (obj->typePtr == &cht_hbytes_type) {
    HBytes_Value *v= OBJ_HBYTES(obj);
    memset(nums,0,sizeof(nums));
    nums[1]= cht_hb_len(v);
  
    if (HBYTES_ISEMPTY(v)) tn= "empty";
    else if (HBYTES_ISSENTINEL(v)) tn= "sentinel!";
    else if (HBYTES_ISSIMPLE(v)) tn= "simple";
    else {
      HBytes_ComplexValue *cx= v->begin_complex;
      tn= "complex";
      nums[0]= cx->prespace;
      nums[2]= cx->avail - cx->len;
    }
    lnl= 3;
  } else {
    tn= "other";
    lnl= 0;
  }
    
  objl[0]= Tcl_NewStringObj((char*)tn,-1);
  for (i=0; i<lnl; i++) objl[i+1]= Tcl_NewIntObj(nums[i]);
  *result= Tcl_NewListObj(lnl+1,objl);
    
  return TCL_OK;
}

static void hbytes_t_dup(Tcl_Obj *src, Tcl_Obj *dup) {
  cht_hb_array(OBJ_HBYTES(dup),
	       cht_hb_data(OBJ_HBYTES(src)),
	       cht_hb_len(OBJ_HBYTES(src)));
  dup->typePtr= &cht_hbytes_type;
}

static void hbytes_t_free(Tcl_Obj *o) {
  cht_hb_free(OBJ_HBYTES(o));
}

void cht_obj_updatestr_array_prefix(Tcl_Obj *o, const Byte *byte,
				int l, const char *prefix) {
  char *str;
  int pl;

  pl= strlen(prefix);
  assert(l < INT_MAX/2 - 1 - pl);
  o->length= l*2+pl;
  str= o->bytes= TALLOC(o->length+1);
  
  memcpy(str,prefix,pl);
  str += pl;

  while (l>0) {
    sprintf(str,"%02x",*byte);
    str+=2; byte++; l--;
  }
  *str= 0;
}

void cht_obj_updatestr_array(Tcl_Obj *o, const Byte *byte, int l) {
  cht_obj_updatestr_array_prefix(o,byte,l,"");
}

static void hbytes_t_ustr(Tcl_Obj *o) {
  cht_obj_updatestr_array(o,
		      cht_hb_data(OBJ_HBYTES(o)),
		      cht_hb_len(OBJ_HBYTES(o)));
}

static int hbytes_t_sfa(Tcl_Interp *ip, Tcl_Obj *o) {
  char *str, *ep;
  Byte *bytes;
  int l;
  char cbuf[3];

  if (o->typePtr == &cht_ulong_type) {
    uint32_t ul;

    ul= htonl(*(const uint32_t*)&o->internalRep.longValue);
    cht_hb_array(OBJ_HBYTES(o), (const Byte*)&ul, 4);

  } else {
  
    str= Tcl_GetStringFromObj(o,&l);  assert(str);
    cht_objfreeir(o);

    if (l & 1) return cht_staticerr(ip, "hbytes: conversion from hex:"
				" odd length in hex", "HBYTES SYNTAX");

    bytes= cht_hb_arrayspace(OBJ_HBYTES(o), l/2);

    cbuf[2]= 0;
    while (l>0) {
      cbuf[0]= *str++;
      cbuf[1]= *str++;
      *bytes++= strtoul(cbuf,&ep,16);
      if (ep != cbuf+2) {
	cht_hb_free(OBJ_HBYTES(o));
	return cht_staticerr(ip, "hbytes: conversion from hex:"
			 " bad hex digit", "HBYTES SYNTAX");
      }
      l -= 2;
    }

  }

  o->typePtr = &cht_hbytes_type;
  return TCL_OK;
}

Tcl_ObjType cht_hbytes_type = {
  "hbytes",
  hbytes_t_free, hbytes_t_dup, hbytes_t_ustr, hbytes_t_sfa
};

int cht_do_hbytes_raw2h(ClientData cd, Tcl_Interp *ip,
		    Tcl_Obj *binary, HBytes_Value *result) {
  const unsigned char *str;
  int l;

  str= Tcl_GetByteArrayFromObj(binary,&l);
  cht_hb_array(result, str, l);
  return TCL_OK;
}

int cht_do_hbytes_h2raw(ClientData cd, Tcl_Interp *ip,
		    HBytes_Value hex, Tcl_Obj **result) {
  *result= Tcl_NewByteArrayObj(cht_hb_data(&hex), cht_hb_len(&hex));
  return TCL_OK;
}

int cht_do_hbytes_length(ClientData cd, Tcl_Interp *ip,
		     HBytes_Value v, int *result) {
  *result= cht_hb_len(&v);
  return TCL_OK;
}

int cht_do_hbytes_random(ClientData cd, Tcl_Interp *ip,
		     int length, HBytes_Value *result) {
  Byte *space;
  int rc;
  
  space= cht_hb_arrayspace(result, length);
  rc= cht_get_urandom(ip, space, length);
  if (rc) { cht_hb_free(result); return rc; }
  return TCL_OK;
}  
  
int cht_do_hbytes_overwrite(ClientData cd, Tcl_Interp *ip,
			HBytes_Var v, int start, HBytes_Value sub) {
  int sub_l;

  sub_l= cht_hb_len(&sub);
  if (start < 0)
    return cht_staticerr(ip, "hbytes overwrite start -ve",
		     "HBYTES LENGTH RANGE");
  if (start + sub_l > cht_hb_len(v.hb))
    return cht_staticerr(ip, "hbytes overwrite out of range",
		     "HBYTES LENGTH UNDERRUN");
  memcpy(cht_hb_data(v.hb) + start, cht_hb_data(&sub), sub_l);
  return TCL_OK;
}

int cht_do_hbytes_trimleft(ClientData cd, Tcl_Interp *ip, HBytes_Var v) {
  const Byte *o, *p, *e;
  o= p= cht_hb_data(v.hb);
  e= p + cht_hb_len(v.hb);

  while (p<e && !*p) p++;
  if (p != o)
    cht_hb_unprepend(v.hb, p-o);

  return TCL_OK;
}

int cht_do_hbytes_repeat(ClientData cd, Tcl_Interp *ip,
		     HBytes_Value sub, int count, HBytes_Value *result) {
  int sub_l;
  Byte *data;
  const Byte *sub_d;

  sub_l= cht_hb_len(&sub);
  if (count < 0) return cht_staticerr(ip, "hbytes repeat count -ve",
				  "HBYTES LENGTH RANGE");
  if (count > INT_MAX/sub_l) return cht_staticerr(ip, "hbytes repeat too long", 0);

  data= cht_hb_arrayspace(result, sub_l*count);
  sub_d= cht_hb_data(&sub);
  while (count) {
    memcpy(data, sub_d, sub_l);
    count--; data += sub_l;
  }
  return TCL_OK;
}  

int cht_do_hbytes_xor(ClientData cd, Tcl_Interp *ip,
		  HBytes_Var v, HBytes_Value d) {
  int l;
  Byte *dest;
  const Byte *source;

  l= cht_hb_len(v.hb);
  if (cht_hb_len(&d) != l) return
    cht_staticerr(ip, "hbytes xor lengths do not match", "HBYTES LENGTH MISMATCH");

  dest= cht_hb_data(v.hb);
  source= cht_hb_data(&d);
  memxor(dest,source,l);
  return TCL_OK;
}
  
int cht_do_hbytes_zeroes(ClientData cd, Tcl_Interp *ip,
		     int length, HBytes_Value *result) {
  Byte *space;
  space= cht_hb_arrayspace(result, length);
  memset(space,0,length);
  return TCL_OK;
}

int cht_do_hbytes_compare(ClientData cd, Tcl_Interp *ip,
		      HBytes_Value a, HBytes_Value b, int *result) {
  int al, bl, minl, r;

  al= cht_hb_len(&a);
  bl= cht_hb_len(&b);
  minl= al<bl ? al : bl;

  r= memcmp(cht_hb_data(&a), cht_hb_data(&b), minl);
  
  if (r<0) *result= -2;
  else if (r>0) *result= +2;
  else {
    if (al<bl) *result= -1;
    else if (al>bl) *result= +1;
    else *result= 0;
  }
  return TCL_OK;
}

int cht_do_hbytes_range(ClientData cd, Tcl_Interp *ip,
		    HBytes_Value v, int start, int size,
		    HBytes_Value *result) {
  const Byte *data;
  int l;

  l= cht_hb_len(&v);
  if (start<0 || size<0)
    return cht_staticerr(ip,"hbytes range subscript(s) -ve","HBYTES LENGTH RANGE");
  if (l<start+size)
    return cht_staticerr(ip, "hbytes range subscripts too big",
		     "HBYTES LENGTH UNDERRUN");

  data= cht_hb_data(&v);
  cht_hb_array(result, data+start, size);
  return TCL_OK;
}

/* hbytes representing uint16_t's */

int cht_do_hbytes_h2ushort(ClientData cd, Tcl_Interp *ip,
		       HBytes_Value hex, long *result) {
  const Byte *data;
  int l;

  l= cht_hb_len(&hex);
  if (l>2)
    return cht_staticerr(ip, "hbytes h2ushort input more than 4 hex digits",
		     "HBYTES VALUE OVERFLOW");

  data= cht_hb_data(&hex);
  *result= data[l-1] | (l>1 ? data[0]<<8 : 0);
  return TCL_OK;
}

int cht_do_hbytes_ushort2h(ClientData cd, Tcl_Interp *ip,
		       long input, HBytes_Value *result) {
  uint16_t us;

  if (input > 0x0ffff)
    return cht_staticerr(ip, "hbytes ushort2h input >2^16",
		     "HBYTES VALUE OVERFLOW");

  us= htons(input);
  cht_hb_array(result,(const Byte*)&us,2);
  return TCL_OK;
}

/* toplevel functions */

CHT_INIT(hbytes,
	 CHTI_TYPE(cht_hbytes_type) CHTI_TYPE(cht_ulong_type),
	 CHTI_COMMANDS(cht_hbytestoplevel_entries))
