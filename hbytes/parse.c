/*
 * hbytes - hex-stringrep efficient byteblocks for Tcl
 * Copyright 2006-2012 Ian Jackson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include "chiark_tcl_hbytes.h"

int cht_pat_hbv(Tcl_Interp *ip, Tcl_Obj *var, HBytes_Var *agg) {
  int rc;
  rc= cht_pat_somethingv(ip,var,&agg->sth,&cht_hbytes_type);
  if (rc) return rc;
  agg->hb= OBJ_HBYTES(agg->sth.obj);
  return TCL_OK;
}
int cht_pat_hb(Tcl_Interp *ip, Tcl_Obj *obj, HBytes_Value *val) {
  int rc;
  rc= Tcl_ConvertToType(ip,obj,&cht_hbytes_type);  if (rc) return rc;
  *val= *OBJ_HBYTES(obj);
  return TCL_OK;
}

Tcl_Obj *cht_ret_hb(Tcl_Interp *ip, HBytes_Value val) {
  Tcl_Obj *obj;
  obj= Tcl_NewObj();
  Tcl_InvalidateStringRep(obj);
  *OBJ_HBYTES(obj)= val;
  obj->typePtr= &cht_hbytes_type;
  return obj;
}
