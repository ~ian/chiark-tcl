/*
 * hbytes - hex-stringrep efficient byteblocks for Tcl
 * Copyright 2006-2012 Ian Jackson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef CHIARK_TCL_HBYTES_H
#define CHIARK_TCL_HBYTES_H

#include "hbytes.h"

typedef struct {
  Byte *start; /* byl bytes */
  Tcl_Obj *data; /* may be 0 to mean empty */
} AddrMap_Entry;

struct AddrMap_Value {
  int byl, used, space;
  AddrMap_Entry *entries;
  /* Entries are sorted by start.  Each entry gives value (or lack of
   * it) for all A st START <= A < NEXT-START.  Last entry has value
   * (or lack of it) for all A >= START.  First entry is always
   * present and always has start all-bits-0. */
}; /* internalRep.otherValuePtr */

#include "hbytes+tcmdif.h"

#endif /*CHIARK_TCL_HBYTES_H*/
