/*
 * cdb, cdb-wr - Tcl bindings for tinycdb and a journalling write extension
 * Copyright 2006-2012 Ian Jackson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include "chiark_tcl_cdb.h"

int cht_cdb_donesomelookup(Tcl_Interp *ip, void *db_v,
			   Tcl_Obj *def, Tcl_Obj **result,
			   const Byte *data, int dlen,
			   int (*storeanswer)(Tcl_Interp *ip, Tcl_Obj **result,
					      const Byte *data, int len)) {
  if (dlen>0) return storeanswer(ip, result, data, dlen);
  if (def) { *result= def; return TCL_OK; }
  return cht_staticerr(ip, "cdbwr lookup key not found", "CDB NOTFOUND");
}

int cht_cdb_storeanswer_string(Tcl_Interp *ip, Tcl_Obj **result,
			       const Byte *data, int len) {
  *result= Tcl_NewStringObj(data, len);
  if (!*result) return cht_staticerr(ip, "Tcl_NewStringObj failed for"
       " lookup (utf-8 encoding problem?)", "CDB BADSTRING");
  return TCL_OK;
}
  
int cht_cdb_storeanswer_hb(Tcl_Interp *ip, Tcl_Obj **result,
			   const Byte *data, int len) {
  HBytes_Value val;
  cht_hb_array(&val, data, len);
  *result= cht_ret_hb(ip, val);
  return TCL_OK;
}

int cht_cdb_lookup_cdb(Tcl_Interp *ip, struct cdb *cdb,
		       const Byte *key, int klen,
		       const Byte **data_r, int *len_r) {
  int r;
  
  r= cdb_find(cdb, key, klen);
  if (!r) { *data_r= 0; *len_r= -1; return TCL_OK; }
  if (r<0) return cht_posixerr(ip, errno, "cdb_find failed");
  assert(r==1);
  *len_r= cdb_datalen(cdb);
  assert(*len_r > 0);
  *data_r= cdb_getdata(cdb);
  if (!*data_r) return cht_posixerr(ip, errno, "cdb_getdata failed");
  return TCL_OK;
}

CHT_INIT(cdb,
	 CHTI_OTHER(hbytes),
	 CHTI_COMMANDS(cht_cdbtoplevel_entries))
