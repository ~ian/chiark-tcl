/*
 * cdb, cdb-wr - Tcl bindings for tinycdb and a journalling write extension
 * Copyright 2006-2012 Ian Jackson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include "chiark_tcl_cdb.h"

typedef struct Ro {
  int ix, fd;
  struct cdb cdb;
} Ro;

static void ro_close(Ro *ro) {
  cdb_free(&ro->cdb);
  close(ro->fd);
}

static void destroy_cdb_idtabcb(Tcl_Interp *ip, void *ro_v) {
  ro_close(ro_v);
  TFREE(ro_v);
}

const IdDataSpec cdbtcl_databases= {
  "cdb-db", "cdb-opendatabases-table", destroy_cdb_idtabcb
};

int cht_do_cdb_open(ClientData cd, Tcl_Interp *ip,
		    const char *path, void **result) {
  Ro *ro;
  int rc, r;

  ro= TALLOC(sizeof(*ro));
  ro->ix= -1;
  ro->fd= open(path, O_RDONLY);
  if (ro->fd<0) PE("open database file");
  r= cdb_init(&ro->cdb, ro->fd);
  if (r) PE("initialise cdb");
  *result= ro;
  return TCL_OK;

 x_rc:
  if (ro->fd >= 0) close(ro->fd);
  return rc;
}

int cht_do_cdb_close(ClientData cd, Tcl_Interp *ip, void *ro_v) {
  ro_close(ro_v);
  cht_tabledataid_disposing(ip, ro_v, &cdbtcl_databases);
  TFREE(ro_v);
  return TCL_OK;
}

int cht_do_cdb_lookup(ClientData cd, Tcl_Interp *ip, void *ro_v,
		      Tcl_Obj *keyo, Tcl_Obj *def, Tcl_Obj **result) {
  Ro *ro= ro_v;
  const Byte *key;
  const Byte *data;
  int r, dlen, klen;

  key= Tcl_GetStringFromObj(keyo, &klen);  assert(key);
  
  r= cht_cdb_lookup_cdb(ip, &ro->cdb, key, klen, &data, &dlen);
  if (r) return r;
  
  return cht_cdb_donesomelookup(ip, ro_v, def, result, data, dlen,
				cht_cdb_storeanswer_string);
}

int cht_do_cdb_lookup_hb(ClientData cd, Tcl_Interp *ip, void *ro_v,
			 HBytes_Value key, Tcl_Obj *def, Tcl_Obj **result) {
  Ro *ro= ro_v;
  const Byte *data;
  int r, dlen;
  
  r= cht_cdb_lookup_cdb(ip, &ro->cdb,
			cht_hb_data(&key), cht_hb_len(&key),
			&data, &dlen);
  if (r) return r;
  
  return cht_cdb_donesomelookup(ip, ro_v, def, result, data, dlen,
				cht_cdb_storeanswer_hb);
}
