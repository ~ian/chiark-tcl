/*
 * cdb, cdb-wr - Tcl bindings for tinycdb and a journalling write extension
 * Copyright 2006-2012 Ian Jackson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CHIARK_TCL_CDB_H
#define CHIARK_TCL_CDB_H

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <ctype.h>
#include <stdio.h>
#include <stddef.h>

#include <cdb.h>

#include "hbytes.h"
#include "cdb+tcmdif.h"

#define RWSCF_OKJUNK 002

extern const IdDataSpec cdbtcl_databases, cdbtcl_rwdatabases;

/*---------- from lookup.c ----------*/

int cht_cdb_donesomelookup(Tcl_Interp *ip, void *db_v,
			   Tcl_Obj *def, Tcl_Obj **result,
			   const Byte *data, int dlen,
			   int (*storeanswer)(Tcl_Interp *ip, Tcl_Obj **result,
					      const Byte *data, int len));
int cht_cdb_storeanswer_string(Tcl_Interp *ip, Tcl_Obj **result,
			       const Byte *data, int len);
int cht_cdb_storeanswer_hb(Tcl_Interp *ip, Tcl_Obj **result,
			   const Byte *data, int len);
int cht_cdb_lookup_cdb(Tcl_Interp *ip, struct cdb *cdb,
		       const Byte *key, int klen,
		       const Byte **data_r, int *dlen_r);

/*---------- macros ----------*/

#define PE(m) do{						\
    rc= cht_posixerr(ip, errno, "failed to " m); goto x_rc;	\
  }while(0)

#endif /*CHIARK_TCL_CDB_H*/
