/*
 * base code for various Tcl extensions
 * Copyright 2006-2012 Ian Jackson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include "chiark-tcl-base.h"

int cht_pat_enum(Tcl_Interp *ip, Tcl_Obj *obj, const void **val,
	     const void *opts, size_t sz, const char *what) {
  *val= cht_enum_lookup_cached_func(ip,obj,opts,sz,what);
  if (!*val) return TCL_ERROR;
  return TCL_OK;
}
  
int cht_pat_obj(Tcl_Interp *ip, Tcl_Obj *obj, Tcl_Obj **val) {
  *val= obj;
  return TCL_OK;
}

Tcl_Obj *cht_ret_int(Tcl_Interp *ip, int val) {
  return Tcl_NewIntObj(val);
}

Tcl_Obj *cht_ret_obj(Tcl_Interp *ip, Tcl_Obj *val) {
  return val;
}

void cht_setstringresult(Tcl_Interp *ip, const char *m) {
  Tcl_ResetResult(ip);
  Tcl_AppendResult(ip, m, (char*)0);
}
