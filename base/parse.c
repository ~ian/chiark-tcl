/*
 * base code for various Tcl extensions
 * Copyright 2006-2012 Ian Jackson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include "chiark-tcl-base.h"

int cht_pat_charfrom(Tcl_Interp *ip, Tcl_Obj *obj, int *val,
		 const char *opts, const char *what) {
  *val= cht_enum1_lookup_cached_func(ip,obj,opts,what);
  if (*val==-1) return TCL_ERROR;
  return TCL_OK;
}

int cht_pat_int(Tcl_Interp *ip, Tcl_Obj *obj, int *val) {
  return Tcl_GetIntFromObj(ip, obj, val);
}
  
int cht_pat_long(Tcl_Interp *ip, Tcl_Obj *obj, long *val) {
  return Tcl_GetLongFromObj(ip, obj, val);
}
  
int cht_pat_string(Tcl_Interp *ip, Tcl_Obj *obj, const char **val) {
  *val= Tcl_GetString(obj);
  return TCL_OK;
}

int cht_pat_constv(Tcl_Interp *ip, Tcl_Obj *var,
	       Tcl_Obj **val_r, Tcl_ObjType *type) {
  int rc;
  Tcl_Obj *val;
  
  val= Tcl_ObjGetVar2(ip,var,0,TCL_LEAVE_ERR_MSG);
  if (!val) return TCL_ERROR;

  if (type) {
    rc= Tcl_ConvertToType(ip,val,type);
    if (rc) return rc;
  }

  *val_r= val;
  return TCL_OK;
}

void cht_init_somethingv(Something_Var *sth) {
  sth->obj=0; sth->var=0; sth->copied=0;
}

int cht_pat_somethingv(Tcl_Interp *ip, Tcl_Obj *var,
		   Something_Var *sth, Tcl_ObjType *type) {
  int rc;
  Tcl_Obj *val;

  sth->var= var;

  val= Tcl_ObjGetVar2(ip,var,0,TCL_LEAVE_ERR_MSG);
  if (!val) return TCL_ERROR;

  rc= Tcl_ConvertToType(ip,val,type);
  if (rc) return rc;

  if (Tcl_IsShared(val)) {
    val= Tcl_DuplicateObj(val);
    sth->copied= 1;
  }
  Tcl_InvalidateStringRep(val);
  sth->obj= val;

  return TCL_OK;
}

void cht_fini_somethingv(Tcl_Interp *ip, int rc, Something_Var *sth) {
  Tcl_Obj *ro;
  
  if (!rc) {
    assert(sth->obj);
    ro= Tcl_ObjSetVar2(ip,sth->var,0,sth->obj,TCL_LEAVE_ERR_MSG);
    if (!ro) rc= TCL_ERROR;
  }
  if (rc && sth->copied)
    Tcl_DecrRefCount(sth->obj);
}

Tcl_Obj *cht_ret_long(Tcl_Interp *ip, long val) {
  return Tcl_NewLongObj(val);
}

Tcl_Obj *cht_ret_string(Tcl_Interp *ip, const char *val) {
  return Tcl_NewStringObj(val,-1);
}
