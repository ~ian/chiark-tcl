/*
 * base code for various Tcl extensions
 * Copyright 2006-2012 Ian Jackson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include "chiark-tcl-base.h"

int cht_staticerr(Tcl_Interp *ip, const char *m, const char *ec) {
  Tcl_SetResult(ip, (char*)m, TCL_STATIC);
  if (ec) Tcl_SetObjErrorCode(ip, Tcl_NewStringObj(ec,-1));
  return TCL_ERROR;
}

int cht_posixerr(Tcl_Interp *ip, int errnoval, const char *m) {
  const char *em;
  
  Tcl_ResetResult(ip);
  errno= errnoval;
  em= Tcl_PosixError(ip);
  Tcl_AppendResult(ip, m, ": ", em, (char*)0);
  return TCL_ERROR;
}

int cht_newfdposixerr(Tcl_Interp *ip, int fd, const char *m) {
  int e;
  e= errno;
  close(fd);
  return cht_posixerr(ip,e,m);
}

void cht_objfreeir(Tcl_Obj *o) {
  if (o->typePtr && o->typePtr->freeIntRepProc)
    o->typePtr->freeIntRepProc(o);
  o->typePtr= 0;
}  

void cht_obj_updatestr_vstringls(Tcl_Obj *o, ...) {
  va_list al;
  char *p;
  const char *part;
  int l;
  size_t pl;

  va_start(al,o);
  for (l=0; (part= va_arg(al, const char*)); ) {
    pl= va_arg(al, size_t);
    assert(pl <= INT_MAX/2 - l);
    l += pl;
  }
  va_end(al);
  
  o->length= l;
  o->bytes= TALLOC(l+1);

  va_start(al,o);
  for (p= o->bytes; (part= va_arg(al, const char*)); p += pl) {
    pl= va_arg(al, size_t);
    memcpy(p, part, pl);
  }
  va_end(al);

  *p= 0;
}

void cht_obj_updatestr_string(Tcl_Obj *o, const char *str) {
  cht_obj_updatestr_vstringls(o, str, strlen(str), (char*)0);
}

#define URANDOM "/dev/urandom"

int cht_get_urandom(Tcl_Interp *ip, Byte *buffer, int l) {
  static FILE *urandom;

  int r;

  if (!urandom) {
    urandom= fopen(URANDOM,"rb");
    if (!urandom) return cht_posixerr(ip,errno,"open " URANDOM);
  }
  r= fread(buffer,1,l,urandom);
  if (r==l) return 0;

  if (ferror(urandom)) {
    r = cht_posixerr(ip,errno,"read " URANDOM);
  } else {
    assert(feof(urandom));
    r = cht_staticerr(ip, URANDOM " gave eof!", 0);
  }
  fclose(urandom); urandom=0;
  return r;
}

void cht_prepare__basic(Tcl_Interp *ip) {
  static int prepared;

  if (prepared) return;
  Tcl_RegisterObjType(&cht_tabledataid_nearlytype);
  Tcl_RegisterObjType(&cht_enum_nearlytype);
  Tcl_RegisterObjType(&cht_enum1_nearlytype);
  prepared= 1;
}

void cht_setup__commands(Tcl_Interp *ip, const TopLevel_Command *cmds) {
  const TopLevel_Command *cmd;
  
  for (cmd= cmds;
       cmd->name;
       cmd++)
    Tcl_CreateObjCommand(ip, (char*)cmd->name, cmd->func, 0,0);
}
