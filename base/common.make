# base code for various Tcl extensions
# Copyright 2006-2012 Ian Jackson
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this library; if not, see <http://www.gnu.org/licenses/>.


VERSION ?=	1
FAMILY ?=	chiark_tcl
TCL_VERSION ?=	8.5
TCL_INCLUDEDIR ?= /usr/include/tcl$(TCL_VERSION)

OPTIMISE ?=		-O2
TCL_MEM_DEBUG ?=	-DTCL_MEM_DEBUG

TCMDIFGEN ?=	$(BASE_DIR)/tcmdifgen
BASE_TCT ?=	$(BASE_DIR)/base.tct
BASE_SHLIB ?=	lib$(FAMILY)-$(VERSION)

CFLAGS +=	-g -Wall -Wmissing-prototypes -Wstrict-prototypes -Werror \
		$(OPTIMISE)

ifeq ($(shell $(CC) -Wno-pointer-sign -E -x c /dev/null >/dev/null || echo x),)
CFLAGS +=	-Wno-pointer-sign
endif

ifeq ($(shell $(CC) -fno-strict-aliasing -E -x c /dev/null >/dev/null || echo x),)
CFLAGS +=	-fno-strict-aliasing
endif

CPPFLAGS +=	-I$(TCL_INCLUDEDIR) -I$(BASE_DIR)
CPPFLAGS +=	$(TCL_MEM_DEBUG)

AUTOS +=	$(AUTO_SRCS) $(AUTO_HDRS)

default:	all

