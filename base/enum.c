/*
 * base code for various Tcl extensions
 * Copyright 2006-2012 Ian Jackson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include "chiark-tcl-base.h"

static void enum_nt_dup(Tcl_Obj *src, Tcl_Obj *dup) {
  dup->internalRep= src->internalRep;
  dup->typePtr= src->typePtr;
}

static void enum_nt_ustr(Tcl_Obj *o) {
  abort();
}

static int enum_nt_sfa(Tcl_Interp *ip, Tcl_Obj *o) {
  abort();
}

Tcl_ObjType cht_enum_nearlytype = {
  "enum-nearly",
  0, enum_nt_dup, enum_nt_ustr, enum_nt_sfa
};

Tcl_ObjType cht_enum1_nearlytype = {
  "enum1-nearly",
  0, enum_nt_dup, enum_nt_ustr, enum_nt_sfa
};

static void report_bad(Tcl_Interp *ip, const char *what, const char *supplied,
		       const void *first, size_t each,
		       int (*isvalid)(const void *entry),
		       void (*appres)(Tcl_Interp *ip, const void *entry)) {
  int count, i;
  const Byte *entry;

  for (entry=first; isvalid(entry); entry+=each);
  count= (entry - (const Byte*)first) / each;

  Tcl_ResetResult(ip);
  Tcl_AppendResult(ip, "bad ",what," \"",supplied,"\": must be",(char*)0);

  for (i=0, entry=first; i<count; i++, entry+=each) {
    Tcl_AppendResult(ip,
		     (char*)(i==0 ? " " :
			     i+1==count ? ", or " :
			     ", "),
		     (char*)0);
    appres(ip,entry);
  }
}

static const char *enum_str(const void *p) { return *(const char*const*)p; }
static int isvalid_enum(const void *p) { return !!enum_str(p); }
static void appres_enum(Tcl_Interp *ip, const void *p) {
  Tcl_AppendResult(ip, enum_str(p), (char*)0);
}

const void *cht_enum_lookup_cached_func(Tcl_Interp *ip, Tcl_Obj *o,
				    const void *firstentry, size_t entrysize,
				    const char *what) {
  const char *supplied, *found;
  const char *ep;
  
  if (o->typePtr == &cht_enum_nearlytype &&
      o->internalRep.twoPtrValue.ptr1 == firstentry)
    return o->internalRep.twoPtrValue.ptr2;

  supplied= Tcl_GetStringFromObj(o,0);  assert(supplied);
  for (ep= firstentry;
       (found= *(const char*const*)ep) && strcmp(supplied,found);
       ep += entrysize);

  if (found) {
    cht_objfreeir(o);
    o->typePtr= &cht_enum_nearlytype;
    o->internalRep.twoPtrValue.ptr1= (void*)firstentry;
    o->internalRep.twoPtrValue.ptr2= (void*)ep;
    return ep;
  }

  report_bad(ip,what,supplied, firstentry,entrysize, isvalid_enum,appres_enum);
  return 0;
}

static int isvalid_enum1(const void *p) { return !!*(const char*)p; }
static void appres_enum1(Tcl_Interp *ip, const void *p) {
  char buf[2];
  buf[0]= *(const char*)p;
  buf[1]= 0;
  Tcl_AppendResult(ip, buf, (char*)0);
}

int cht_enum1_lookup_cached_func(Tcl_Interp *ip, Tcl_Obj *o,
			     const char *opts, const char *what) {
  const char *supplied, *fp;
  
  if (o->typePtr != &cht_enum1_nearlytype ||
      o->internalRep.twoPtrValue.ptr1 != opts) {

    supplied= Tcl_GetStringFromObj(o,0);  assert(supplied);
    
    if (!(strlen(supplied) == 1 &&
	  (fp= strchr(opts, supplied[0])))) {
      report_bad(ip,what,supplied, opts,1, isvalid_enum1,appres_enum1);
      return -1;
    }
    
    cht_objfreeir(o);
    o->typePtr= &cht_enum1_nearlytype;
    o->internalRep.twoPtrValue.ptr1= (void*)opts;
    o->internalRep.twoPtrValue.ptr2= (void*)fp;
  }
  return (const char*)o->internalRep.twoPtrValue.ptr2 - opts;
}
