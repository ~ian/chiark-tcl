# base code for various Tcl extensions
# Copyright 2006-2012 Ian Jackson
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this library; if not, see <http://www.gnu.org/licenses/>.



EXTPREFIX ?=	$(FAMILY)_
EXTENSION ?=	$(EXTPREFIX)$(EXTBASE)
SHLIB ?=	$(EXTENSION)-$(VERSION)
TABLE ?=	$(EXTBASE)

AUTO_HDRS +=	$(TABLE)+tcmdif.h
AUTO_SRCS +=	$(TABLE)+tcmdif.c
CFILES +=	$(TABLE)+tcmdif

CPPFLAGS += $(foreach o, $(OTHER_EXTS), -I../$(dir $o))
LDLIBS += $(foreach o, $(OTHER_EXTS), ../$(dir $o)$(EXTPREFIX)$(notdir $o)-$(VERSION).so)

LDLIBS +=	$(BASE_DIR)/$(BASE_SHLIB).so

include		$(BASE_DIR)/common.make
include		$(BASE_DIR)/shlib.make

TCMDIFARGS ?=	-p$(FAMILY)_$(EXTBASE) -o$@ $(BASE_TCT) $(OTHER_TCTS) $<

%+tcmdif.c:	%.tct $(BASE_TCT) $(OTHER_TCTS) $(TCMDIFGEN)
		$(TCMDIFGEN) -wc $(TCMDIFARGS)

%+tcmdif.h:	%.tct $(BASE_TCT) $(OTHER_TCTS) $(TCMDIFGEN)
		$(TCMDIFGEN) -wh $(TCMDIFARGS)

OTHER_DIRS +=	$(BASE_DIR)
OTHER_DIRS +=	$(addprefix ../,$(dir $(OTHER_EXTS)))
OTHER_DIRS +=	.

null :=
space := $(null) #

test-load.tcl:
		echo >$@ "load $(SHLIB).so"

test-load:	$(SHLIB).so test-load.tcl
		@set -x; LD_LIBRARY_PATH=$(subst $(space),:,$(strip $(OTHER_DIRS)))$${LD_LIBRARY_PATH+:}$${LD_LIBRARY_PATH} \
		tclsh$(TCL_VERSION) test-load.tcl

debian-substvars: all
		set -e; d=`pwd`; cd ..; \
		dpkg-shlibdeps -T"$$d"/$@ "$$d"/$(SHLIB).so

include		$(BASE_DIR)/final.make
