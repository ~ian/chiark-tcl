/*
 * adns lookup TYPE DOMAIN [QUERY-OPTIONS]                    => [list RDATA]
 *    if no or dontknow, throws an exception, with errorCode one of
 *         ADNS permfail 300 nxdomain {No such domain}
 *         ADNS permfail 301 nodata {No such data}
 *         ADNS tempfail ERROR-CODE ERROR-NAME ERROR-STRING
 *    where
 *         ERROR-CODE is the numerical adns status value
 *         ERROR-NAME is the symbolic adns status value (in lowercase)
 *         ERROR-STRING is the result of adns_strstatus
 *
 * adns synch TYPE DOMAIN [QUERY-OPTIONS]                     => RESULTS
 *        RESULTS is [list ok|permfail|tempfail
 *                         ERROR-CODE ERROR-NAME ERROR-STRING  \
 *                         OWNER CNAME                         \
 *                         [list RDATA ...]]
 *        OWNER is the RR owner
 *        CNAME is the empty string or the canonical name if we went
 *                  via a CNAME
 *
 * adns asynch ON-YES ON-NO ON-DONTKNOW XARGS \
 *             TYPE DOMAIN \
 *             [QUERY-OPTIONS...]                            => QUERY-ID
 *        calls, later,
 *           [concat ON-YES|ON-NO|ON-DONTKNOW XARGS RESULTS]
 * adns asynch-cancel QUERY-ID
 *
 * QUERY-OPTIONS are zero or more of
 *         -resolver RESOLVER  (see adns new-resolver)
 *                 default is to use a default resolver
 *         -search
 *         -usevc
 *         -quoteok-query
 *         -quoteok-anshost
 *         -quotefail-cname
 *         -cname-loose
 *         -cname-forbid
 *         -reverse
 *         -reverse-any ZONE-A-LIKE
 *
 * adns new-resolver [RES-OPTIONS...]                         => RESOLVER
 *        options:
 *         -errfile stdout|stderr       (stderr is the default)
 *         -noerrprint
 *         -errcallback CALLBACK    results in  eval CALLBACK [list MESSAGE]
 *         -noenv|-debug|-logpid
 *         -checkc-entex
 *         -checkc-freq
 *         -config CONFIG-STRING
 *
 * adns set-default-resolver RESOLVER
 *        cancels any outstanding queries from a previous anonymous
 *        default resolver
 *
 * adns destroy-resolver RESOLVER
 *        cancels outstanding queries
 *
 */
/* ---8<--- end of documentation comment --8<-- */

/*
 * adns.c - adns binding for Tcl
 * Copyright 2006-2012 Ian Jackson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE

#include <stdio.h>

#include <adns.h>

#include "chiark_tcl_adns.h"

/*---------- important types and forward declarations ----------*/

typedef struct Query Query;
typedef struct Resolver Resolver;
typedef struct OptionInfo OptionInfo;

static void asynch_sethandlers(Resolver *res);
static void asynch_cancelhandlers(Resolver *res);
static void asynch_perturbed(Resolver *res);

static void asynch_query_dispose(Tcl_Interp *interp, Query *query);

#define ASSOC_DEFAULTRES "adns-defaultresolver"

/*---------- common resolver/query option processing ----------*/

typedef struct {
  /* this struct type is used to hold both resolver and query options */
  /* common to resolver and query: */
  unsigned long aflags;
  unsigned long sflags;
  /* resolver: */
  FILE *errfile;
  Tcl_Obj *errcallback;
  const char *config_string;
  /* query: */
  Resolver *resolver;
  const char *reverseany;
} OptionParse;

struct OptionInfo {
  const char *name;
  int (*fn)(Tcl_Interp *ip, const OptionInfo *oi, Tcl_Obj *arg,
	    OptionParse *op);
  int takesarg;
  unsigned long flags_add, flags_remove;
};

enum {
  oisf_reverse=     0x0002
};

static int oiufn_f(const OptionInfo *oi, unsigned long *flags) {
  *flags &= ~oi->flags_remove;
  *flags |= oi->flags_add;
  return TCL_OK;
}
static int oifn_fa(Tcl_Interp *ip, const OptionInfo *oi, Tcl_Obj *arg,
		   OptionParse *op) { return oiufn_f(oi,&op->aflags); }
static int oifn_fs(Tcl_Interp *ip, const OptionInfo *oi, Tcl_Obj *arg,
		   OptionParse *op) { return oiufn_f(oi,&op->sflags); }

static int oifn_reverse_any(Tcl_Interp *ip, const OptionInfo *oi,
			    Tcl_Obj *arg, OptionParse *op) {
  return cht_pat_string(ip,arg,&op->reverseany);
}

#define OIFA1(t,f,r) { "-" #f, oifn_fa, 0, adns_##t##_##f, r }
#define OIFA2(t,f,g) { "-" #f "-" #g, oifn_fa, 0, adns_##t##_##f##_##g, 0 }
#define OIFS(f) { "-" #f, oifn_fs, 0, oisf_##f, 0 }
#define OICA(o) { "-" #o, oifn_##o, 1 }

static void optparse_blank(OptionParse *op) {
  memset(op,0,sizeof(*op));
  op->errfile= stderr;
  op->errcallback= 0;
  op->config_string= 0;
}

static int parse_options(Tcl_Interp *ip, int objc, Tcl_Obj *const *objv,
			 const OptionInfo opttable[], OptionParse *op) {
  const OptionInfo *oi;
  const void *oi_v;
  Tcl_Obj *arg;
  int rc;

  objc--; objv++;
  for (;;) {
    if (!objc--) break;
    rc= cht_pat_enum(ip, *objv++, &oi_v, opttable, sizeof(OptionInfo),
		 "query or resolver option");
    if (rc) return rc;
    oi= oi_v;

    if (oi->takesarg) {
      if (!objc--) {
	cht_setstringresult(ip,"missing value for option");
	return TCL_ERROR;
      }
      arg= *objv++;
    } else {
      arg= 0;
    }
    rc= oi->fn(ip,oi,arg,op);
    if (rc) return rc;
  }
  return TCL_OK;
}

/*---------- resolver management ----------*/

struct Resolver {
  int ix; /* first! */
  Tcl_Interp *interp;
  adns_state ads;
  Tcl_TimerToken timertoken;
  int maxfd;
  fd_set handling[3];
  ScriptToInvoke errcallback;
  Tcl_Obj *errstring_accum;
};

struct Query {
  int ix; /* first! */
  Resolver *res;
  adns_query aqu;
  ScriptToInvoke on_yes, on_no, on_fail;
  Tcl_Obj *xargs;
};

/* The default resolver is recorded using Tcl_SetAssocData with key
 * ASSOC_DEFAULTRES to record the Resolver*.  If it was explicitly
 * created with `adns new-resolver' then ix will be >=0, and the
 * resolver will be destroyed - leaving no default - when explicitly
 * requested.  If it was implicitly created (by starting a query when
 * there is no default) then ix will be -1.  */

static int oifn_errfile(Tcl_Interp *ip, const OptionInfo *oi,
			Tcl_Obj *arg, OptionParse *op) {
  int rc;
  const char *str;
  
  rc= cht_pat_string(ip,arg,&str);  if (rc) return rc;
  if (!strcmp(str,"stderr")) op->errfile= stderr;
  else if (!strcmp(str,"stdout")) op->errfile= stdout;
  else return cht_staticerr(ip,"-errfile argument must be stderr or stdout",0);

  op->aflags &= ~adns_if_noerrprint;
  op->errcallback= 0;
  return TCL_OK;
}

static int oifn_errcallback(Tcl_Interp *ip, const OptionInfo *oi,
			    Tcl_Obj *arg, OptionParse *op) {
  op->errcallback= arg;
  op->aflags &= ~adns_if_noerrprint;
  op->errfile= 0;
  return TCL_OK;
}

static int oifn_config(Tcl_Interp *ip, const OptionInfo *oi,
		       Tcl_Obj *arg, OptionParse *op) {
  return cht_pat_string(ip,arg,&op->config_string);
}

static const OptionInfo resolver_optioninfos[]= {
  OIFA1(if,noenv, 0),
  OIFA1(if,debug, adns_if_noerrprint),
  OIFA1(if,logpid, adns_if_noerrprint),
  OIFA1(if,noerrprint, adns_if_debug),
  OIFA2(if,checkc,entex),
  OIFA2(if,checkc,freq),
  OICA(errfile),
  OICA(errcallback),
  OICA(config),
  { 0 }
};

static void adnslogfn_flushmessage(Resolver *res) {
  cht_scriptinv_invoke(&res->errcallback, 1, &res->errstring_accum);
  Tcl_SetObjLength(res->errstring_accum, 0);
}

static void adnslogfn_callback(adns_state ads, void *logfndata,
			       const char *fmt, va_list al) {
  Resolver *res= logfndata;
  int l, newline;
  char *str;

  l= vasprintf(&str,fmt,al);
  if (l<0) {
    cht_posixerr(res->interp,errno,"construct adns log callback string");
    Tcl_BackgroundError(res->interp);
  }

  if (l==0) { free(str); return; }
  if ((newline= l>0 && str[l-1]=='\n')) l--;

  if (!res->errstring_accum) {
    res->errstring_accum= Tcl_NewStringObj(str,l);
    Tcl_IncrRefCount(res->errstring_accum);
  } else {
    Tcl_AppendToObj(res->errstring_accum,str,l);
  }
  free(str);

  if (newline)
    adnslogfn_flushmessage(res);
}

static Resolver *default_resolver(Tcl_Interp *ip) {
  return Tcl_GetAssocData(ip,ASSOC_DEFAULTRES,0);
}

static void destroy_resolver(Tcl_Interp *ip, Resolver *res) {
  void *query_v;
  Query *query;
  int logstring_len;
  char *rstr;
  adns_query aqu;
 
  if (res == default_resolver(ip))
    Tcl_DeleteAssocData(ip,ASSOC_DEFAULTRES);
 
  if (res->errstring_accum) {
    rstr= Tcl_GetStringFromObj(res->errstring_accum, &logstring_len);
    assert(rstr);
    if (logstring_len)
      adnslogfn_flushmessage(res);
  }

  if (res->ads) {
    /* although adns would throw these away for us, we need to
     * destroy our own data too and only adns has a list of them */
    for (;;) {
      adns_forallqueries_begin(res->ads);
      aqu= adns_forallqueries_next(res->ads, &query_v);
      if (!aqu) break;
      query= query_v;
      assert(query->aqu == aqu);
      query->aqu= 0; /* avoid disrupting the adns query list */
      asynch_query_dispose(ip, query_v);
    }
    adns_finish(res->ads);
    res->ads= 0;
  }
  asynch_cancelhandlers(res);
  cht_scriptinv_cancel(&res->errcallback);
  Tcl_EventuallyFree(res, Tcl_Free);
}

static void destroy_resolver_idtabcb(Tcl_Interp *ip, void *resolver_v) {
  destroy_resolver(ip,resolver_v);
}
static void destroy_resolver_defcb(ClientData resolver_v, Tcl_Interp *ip) {
  destroy_resolver(ip,resolver_v);
}

int cht_do_adns_destroy_resolver(ClientData cd, Tcl_Interp *ip, void *res_v) {
  cht_tabledataid_disposing(ip,res_v,&cht_adnstcl_resolvers);
  destroy_resolver(ip,res_v);
  return TCL_OK;
}

static int create_resolver(Tcl_Interp *ip, const OptionParse *op,
			   Resolver **res_r) {
  Resolver *res=0;
  int rc, i, ec;
  
  res= TALLOC(sizeof(*res));  assert(res);
  res->ix= -1;
  res->interp= ip;
  res->ads= 0;
  res->timertoken= 0;
  res->maxfd= 0;
  for (i=0; i<3; i++) FD_ZERO(&res->handling[i]);
  cht_scriptinv_init(&res->errcallback);
  res->errstring_accum= 0;

  if (op->errcallback)
    cht_scriptinv_set(&res->errcallback, ip, op->errcallback, 0);

  ec= adns_init_logfn(&res->ads,
		      op->aflags | adns_if_noautosys,
		      op->config_string,
		      op->errcallback ? adnslogfn_callback : 0,
		      op->errcallback ? (void*)res : (void*)op->errfile);
  if (ec) { rc= cht_posixerr(ip,ec,"create adns resolver"); goto x_rc; }

  *res_r= res;
  return TCL_OK;

 x_rc:
  if (res) {
    if (res->ads) adns_finish(res->ads);
    TFREE(res);
  }
  return rc;
}  

int cht_do_adns_new_resolver(ClientData cd, Tcl_Interp *ip,
			 int objc, Tcl_Obj *const *objv,
			 void **result) {
  OptionParse op;
  Resolver *res=0;
  int rc;

  optparse_blank(&op);
  rc= parse_options(ip,objc,objv,resolver_optioninfos,&op);
  if (rc) return rc;

  if (op.aflags & adns_if_noerrprint) {
    op.errfile= 0;
    op.errcallback= 0;
  }

  rc= create_resolver(ip, &op, &res);
  if (rc) return rc;

  *result= res;
  return TCL_OK;
}

int cht_do_adns_set_default_resolver(ClientData cd, Tcl_Interp *ip, void *res_v) {
  Resolver *res= res_v;
  Tcl_DeleteAssocData(ip,ASSOC_DEFAULTRES);
  Tcl_SetAssocData(ip, ASSOC_DEFAULTRES, 0, res);
  return TCL_OK;
}

const IdDataSpec cht_adnstcl_resolvers= {
  "adns-res", "adns-resolvers-table", destroy_resolver_idtabcb
};

/*---------- query, query option and answers - common stuff ----------*/

#define RRTYPE_EXACTLY(t) { #t, adns_r_##t }
#define RRTYPE_RAW(t) { #t, adns_r_##t##_raw }
#define RRTYPE_PLUS(t) { #t "+", adns_r_##t }
#define RRTYPE_MINUS(t) { #t "-", adns_r_##t##_raw }

const AdnsTclRRTypeInfo cht_adnstclrrtypeinfo_entries[]= {
  RRTYPE_EXACTLY(a),
  RRTYPE_EXACTLY(cname),
  RRTYPE_EXACTLY(hinfo),
  RRTYPE_EXACTLY(addr),

  RRTYPE_RAW(ns),
  RRTYPE_RAW(mx),
  RRTYPE_EXACTLY(txt),

  RRTYPE_EXACTLY(soa),
  RRTYPE_EXACTLY(ptr),
  RRTYPE_EXACTLY(rp),

  RRTYPE_MINUS(soa),
  RRTYPE_MINUS(ptr),
  RRTYPE_MINUS(rp),
  { 0 }
};

static int oifn_resolver(Tcl_Interp *ip, const OptionInfo *oi,
			 Tcl_Obj *arg, OptionParse *op) {
  void *val_v;
  int rc;
  
  rc= cht_pat_iddata(ip,arg,&val_v,&cht_adnstcl_resolvers);
  if (rc) return rc;
  op->resolver= val_v;
  return TCL_OK;
}

static const OptionInfo query_optioninfos[]= {
  OIFA1(qf,search,0),
  OIFA1(qf,usevc,0),
  OIFA2(qf,quoteok,query),
  OIFA2(qf,quoteok,anshost),
  OIFA2(qf,quotefail,cname),
  OIFA2(qf,cname,loose),
  OIFA2(qf,cname,forbid),
  OICA(resolver),
  OIFS(reverse),
  { "-reverse-any", oifn_reverse_any, 1 },
  { 0 }
};

static int query_submit(Tcl_Interp *ip,
			const AdnsTclRRTypeInfo *type, const char *domain,
			int queryopts_objc, Tcl_Obj *const *queryopts_objv,
			adns_query *aqu_r, void *context, Resolver **res_r) {
  struct sockaddr sa;
  static const int aftry[]= { AF_INET, AF_INET6 };
  OptionParse op;
  OptionParse res_op;
  int rc, r, ec;
  adns_state ads;
  
  op.aflags= adns_qf_owner;
  op.sflags= 0;
  op.resolver= 0;
  op.reverseany= 0;
  rc= parse_options(ip, queryopts_objc,queryopts_objv, query_optioninfos,&op);
  if (rc) return rc;

  if (!op.resolver) {
    op.resolver= default_resolver(ip);
    if (!op.resolver) {
      optparse_blank(&res_op);
      rc= create_resolver(ip, &res_op, &op.resolver);
      if (rc) return rc;

      Tcl_SetAssocData(ip, ASSOC_DEFAULTRES,
		       destroy_resolver_defcb, op.resolver);
    }
  }

  *res_r= op.resolver;
  
  if (op.reverseany || (op.sflags & oisf_reverse)) {
    const int *af;
    for (af=aftry; af < af + sizeof(aftry)/sizeof(*aftry); af++) {
      memset(&sa,0,sizeof(sa));
      sa.sa_family= *af;
      r= inet_pton(*af,domain,&sa);
      if (!r) goto af_found;
    }
    return cht_staticerr(ip,"invalid address for adns reverse submit",
			 "ADNS REVERSE INVALID");
  af_found:;
  }

  ads= op.resolver->ads;

  if (op.reverseany) {
    ec= adns_submit_reverse_any(ads, &sa, op.reverseany,
				type->number, op.aflags, context, aqu_r);
  } else if (op.sflags & oisf_reverse) {
    ec= adns_submit_reverse(ads, &sa,
			    type->number, op.aflags, context, aqu_r);
  } else {
    ec= adns_submit(ads, domain,
		    type->number, op.aflags, context, aqu_r);
  }
  if (ec)
    return cht_posixerr(ip,ec,"submit adns query");

  return TCL_OK;
}

#define RESULTSTATUS_LLEN 4
#define RESULTLIST_LLEN 7

static void make_resultstatus(Tcl_Interp *ip, adns_status status,
			      Tcl_Obj *results[RESULTSTATUS_LLEN]) {
  results[0]= cht_ret_string(ip, adns_errtypeabbrev(status));
  results[1]= cht_ret_int(ip, status);
  results[2]= cht_ret_string(ip, adns_errabbrev(status));
  results[3]= cht_ret_string(ip, adns_strerror(status));
  assert(RESULTSTATUS_LLEN==4);
}

static Tcl_Obj *make_resultrdata(Tcl_Interp *ip, adns_answer *answer) {
  Tcl_Obj **rdata, *rl;
  int i, rrsz;
  adns_status st;
  char *datap, *rdatastring;
  
  rdata= TALLOC(sizeof(*rdata) * answer->nrrs);
  for (i=0, datap=answer->rrs.untyped;
       i<answer->nrrs;
       i++, datap += rrsz) {
    st= adns_rr_info(answer->type, 0,0, &rrsz, datap, &rdatastring);
    assert(!st);
    rdata[i]= cht_ret_string(ip, rdatastring);
    free(rdatastring);
  }
  rl= Tcl_NewListObj(answer->nrrs, rdata);
  TFREE(rdata);
  return rl;
}

static void make_resultlist(Tcl_Interp *ip, adns_answer *answer,
			    Tcl_Obj *results[RESULTLIST_LLEN]) {

  make_resultstatus(ip, answer->status, results);
  assert(RESULTSTATUS_LLEN==4);
  results[4]= cht_ret_string(ip, answer->owner);
  results[5]= cht_ret_string(ip, answer->cname ? answer->cname : "");
  results[6]= make_resultrdata(ip, answer);
  assert(RESULTLIST_LLEN==7);
}

/*---------- synchronous query handling ----------*/

static int synch(Tcl_Interp *ip, const AdnsTclRRTypeInfo *rrtype,
		 const char *domain,
		 int objc, Tcl_Obj *const *objv, adns_answer **answer_r) {
  adns_query aqu;
  Resolver *res;
  int rc, ec;
  
  rc= query_submit(ip,rrtype,domain,objc,objv,&aqu,0,&res);
  if (rc) return rc;

  ec= adns_wait(res->ads,&aqu,answer_r,0);
  assert(!ec);

  asynch_perturbed(res);
  return TCL_OK;
}

int cht_do_adns_lookup(ClientData cd, Tcl_Interp *ip,
		   const AdnsTclRRTypeInfo *rrtype,
		   const char *domain,
		   int objc, Tcl_Obj *const *objv,
		   Tcl_Obj **result) {
  int rc;
  adns_answer *answer;
  
  rc= synch(ip,rrtype,domain,objc,objv,&answer);  if (rc) return rc;

  if (answer->status) {
    Tcl_Obj *problem[RESULTSTATUS_LLEN];
    make_resultstatus(ip, answer->status, problem);
    *result= Tcl_NewListObj(RESULTSTATUS_LLEN, problem);
  } else {
    *result= make_resultrdata(ip, answer);
  }
  free(answer);
  return TCL_OK;
}

int cht_do_adns_synch(ClientData cd, Tcl_Interp *ip,
		  const AdnsTclRRTypeInfo *rrtype,
		  const char *domain,
		  int objc, Tcl_Obj *const *objv,
		  Tcl_Obj **result) {
  int rc;
  adns_answer *answer;
  Tcl_Obj *results[RESULTLIST_LLEN];

  rc= synch(ip,rrtype,domain,objc,objv,&answer);  if (rc) return rc;
  make_resultlist(ip,answer,results);
  free(answer);
  *result= Tcl_NewListObj(RESULTLIST_LLEN, results);
  return TCL_OK;
}

/*---------- asynchronous query handling ----------*/

static void asynch_check_now(Resolver *res);

static void asynch_timerhandler(void *res_v) {
  Resolver *res= res_v;
  res->timertoken= 0;
  adns_processtimeouts(res->ads,0);
  asynch_check_now(res);
}

static void asynch_filehandler(void *res_v, int mask) {
  Resolver *res= res_v;
  int ec;
  
  ec= adns_processany(res->ads);
  if (ec) adns_globalsystemfailure(res->ads);
  asynch_check_now(res);
}

static void asynch_sethandlers_generic(Resolver *res,
				       int shutdown /*from _cancelhandlers*/,
				       int immediate /*from _perturbed*/) {
  fd_set want[3];
  int maxfd;
  struct timeval tv_buf, *timeout;
  int i, fd;

  timeout= 0;
  maxfd= 0;
  for (i=0; i<3; i++) FD_ZERO(&want[i]);

  if (!shutdown)
    adns_beforeselect(res->ads,&maxfd,&want[0],&want[1],&want[2],
		      &timeout,&tv_buf,0);

  for (fd= 0; fd < maxfd || fd < res->maxfd; fd++)
    for (i=0; i<3; i++)
      if (!!FD_ISSET(fd, &res->handling[i])
	  != !!FD_ISSET(fd, &want[i])) {
	int mask=0;
	if (FD_ISSET(fd, &want[0])) mask |= TCL_READABLE;
	if (FD_ISSET(fd, &want[1])) mask |= TCL_WRITABLE;
	if (FD_ISSET(fd, &want[2])) mask |= TCL_EXCEPTION;
	if (mask) {
	  Tcl_CreateFileHandler(fd,mask,asynch_filehandler,res);
	  FD_SET(fd, &res->handling[i]);
	} else {
	  Tcl_DeleteFileHandler(fd);
	  FD_CLR(fd, &res->handling[i]);
	}
      }
  res->maxfd= maxfd;

  Tcl_DeleteTimerHandler(res->timertoken);

  if (immediate) {
    res->timertoken= Tcl_CreateTimerHandler(0,asynch_timerhandler,res);
  } else if (timeout) {
    int milliseconds;

    if (timeout->tv_sec >= INT_MAX/1000 - 1)
      milliseconds= INT_MAX;
    else
      milliseconds= timeout->tv_sec * 1000 +
	(timeout->tv_usec + 999) / 1000;
    
    res->timertoken=
      Tcl_CreateTimerHandler(milliseconds,asynch_timerhandler,res);
  }
}

static void asynch_sethandlers(Resolver *res) {
  asynch_sethandlers_generic(res,0,0);
}
static void asynch_cancelhandlers(Resolver *res) {
  asynch_sethandlers_generic(res,1,0);
}
static void asynch_perturbed(Resolver *res) {
  asynch_sethandlers_generic(res,0,1);
}

static void asynch_check_now(Resolver *res) {
  Tcl_Interp *interp= res->interp;
  adns_query aqu;
  adns_answer *answer;
  void *query_v;
  Query *query;
  ScriptToInvoke *si;
  int ec;
  Tcl_Obj *results[RESULTLIST_LLEN];

  Tcl_Preserve(res);
  
  for (;;) {
    if (!res->ads) { /* oh, it has been destroyed! */
      Tcl_Release(res);
      return;
    }

    aqu= 0;
    ec= adns_check(res->ads, &aqu, &answer, &query_v);
    if (ec==ESRCH || ec==EAGAIN) break;
    assert(!ec);
    query= query_v;

    query->aqu= 0;
    cht_tabledataid_disposing(interp, query, &cht_adnstcl_queries);

    si= (!answer->status ? &query->on_yes
	 : answer->status > adns_s_max_tempfail ? &query->on_no
	 : &query->on_fail);

    make_resultlist(interp, answer, results);
    free(answer);
    cht_scriptinv_invoke(si, RESULTLIST_LLEN, results);
    asynch_query_dispose(interp, query);
  }

  asynch_sethandlers(res);

  Tcl_Release(res);
}
    
int cht_do_adns_asynch(ClientData cd, Tcl_Interp *ip,
		   Tcl_Obj *on_yes, Tcl_Obj *on_no,
		   Tcl_Obj *on_fail, Tcl_Obj *xargs,
		   const AdnsTclRRTypeInfo *rrtype, const char *domain,
		   int objc, Tcl_Obj *const *objv, void **result) {
  Query *query;
  int rc;
  Resolver *res= 0;
  
  query= TALLOC(sizeof(*query));
  query->ix= -1;
  query->aqu= 0;
  cht_scriptinv_init(&query->on_yes);
  cht_scriptinv_init(&query->on_no);
  cht_scriptinv_init(&query->on_fail);
  query->xargs= 0;

  rc= query_submit(ip,rrtype,domain,objc,objv,&query->aqu,query,&query->res);
  if (rc) goto x_rc;

  res= query->res;

  rc= cht_scriptinv_set(&query->on_yes, ip,on_yes, xargs);  if (rc) goto x_rc;
  rc= cht_scriptinv_set(&query->on_no,  ip,on_no,  xargs);  if (rc) goto x_rc;
  rc= cht_scriptinv_set(&query->on_fail,ip,on_fail,xargs);  if (rc) goto x_rc;
  query->xargs= xargs;
  Tcl_IncrRefCount(xargs);
  *result= query;
  query= 0; /* do not dispose */
  rc= TCL_OK;

 x_rc:
  if (query) asynch_query_dispose(ip, query);
  if (res) asynch_perturbed(res);
  return rc;
}

int cht_do_adns_asynch_cancel(ClientData cd, Tcl_Interp *ip, void *query_v) {
  Query *query= query_v;
  Resolver *res= query->res;
  asynch_query_dispose(ip, query);
  asynch_perturbed(res);
  return TCL_OK;
}

static void asynch_query_dispose(Tcl_Interp *interp, Query *query) {
  cht_tabledataid_disposing(interp, query, &cht_adnstcl_queries);
  cht_scriptinv_cancel(&query->on_yes);
  cht_scriptinv_cancel(&query->on_no);
  cht_scriptinv_cancel(&query->on_fail);
  if (query->xargs) Tcl_DecrRefCount(query->xargs);
  if (query->aqu) adns_cancel(query->aqu);
  TFREE(query);
}

static void destroy_query_idtabcb(Tcl_Interp *interp, void *query_v) {
  asynch_query_dispose(interp, query_v);
}

const IdDataSpec cht_adnstcl_queries= {
  "adns", "adns-query-table", destroy_query_idtabcb
};

/*---------- main hooks for tcl ----------*/

CHT_INIT(adns, {}, CHTI_COMMANDS(cht_adnstoplevel_entries))
