/*
 * adns binding for Tcl
 * Copyright 2006-2012 Ian Jackson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ADNSTCL_H
#define ADNSTCL_H

#include "chiark-tcl.h"

typedef struct {
  const char *name;
  adns_rrtype number;
} AdnsTclRRTypeInfo;

extern const IdDataSpec cht_adnstcl_queries, cht_adnstcl_resolvers;

#include "adns+tcmdif.h"

#endif /*ADNSTCL_H*/
