Source: chiark-tcl
Maintainer: Ian Jackson <ijackson@chiark.greenend.org.uk>
Priority: optional
Section: interpreters
Standards-Version: 3.9.1
Build-Depends: libadns1-dev (>= 1.2), nettle-dev, libcdb-dev | tinycdb (<= 0.75), tcl8.6-dev | tcl8.5-dev | tcl8.4-dev | tcl8.3-dev | tcl8.2-dev | tcl-dev, debhelper (>= 5)

Package: libtcl-chiark-1
Architecture: any
Description: Tcl interfaces for adns, cdb, crypto, etc.
 Tcl bindings for:
  * adns (resolver library)
  * cdb (constant database) plus journalling writable database
  * crypto: the nettle cryptographic library
  * hbytes: bytestrings with hex as string representation but efficient
  * dgram: datagram sockets
  * tuntap: tun/tap interfaces
  * maskmap: address masks and maps
 To make sensible use of these you will need a version of Tcl installed
 (this package is compatible with at least Tcl 8.0 to 8.4 inclusive).
 To use the adns and nettle bindings you need to have the
 appropriate libraries installed too.
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-arch: same


# chiark-tcl - various Tcl bindings and extensions
# Copyright 2006-2012 Ian Jackson
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this library; if not, see <http://www.gnu.org/licenses/>.
