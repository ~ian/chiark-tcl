/*
 * crypto - Tcl bindings for parts of the `nettle' crypto library
 * Copyright 2006-2012 Ian Jackson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 */


#include "chiark_tcl_crypto.h"

const PadOp cht_padop_entries[]= {
  { "un", 0, 0 },
  { "ua", 0, 1 },
  { "pn", 1, 0 },
  { "pa", 1, 1 },
  { 0 }
};

typedef struct {
  HBytes_Value *hb;
  int pad, blocksize; /* 0 or 1 */
} PadMethodClientData;

int cht_do_hbcrypto_pad(ClientData cd, Tcl_Interp *ip, const PadOp *op,
		  HBytes_Var v, Tcl_Obj *blocksz, const PadMethodInfo *meth,
		  int methargsc, Tcl_Obj *const *methargsv) {
  PadMethodClientData pmcd;
  int rc;
  
  if (op->use_algname) {
    const BlockCipherAlgInfo *alg;
    alg= enum_lookup_cached(ip,blocksz, cht_blockcipheralginfo_entries,
			    "blockcipher alg for pad");
    if (!alg) return TCL_ERROR;
    pmcd.blocksize= alg->blocksize;
  } else {
    rc= Tcl_GetIntFromObj(ip, blocksz, &pmcd.blocksize);  if (rc) return rc;
    if (pmcd.blocksize < 1) cht_staticerr(ip, "block size must be at least 1", 0);
  }

  pmcd.hb= v.hb;
  pmcd.pad= op->pad;

  return meth->func(&pmcd,ip,methargsc,methargsv);
}
  
int cht_do_padmethodinfo_rfc2406(ClientData cd, Tcl_Interp *ip,
			     Tcl_Obj *nxthdr_arg, int *ok) {
  const PadMethodClientData *pmcd= (const void*)cd;
  int i, rc, padlen, old_len;
  
  if (pmcd->blocksize > 256)
    return cht_staticerr(ip, "block size too large for RFC2406 padding", 0);

  if (pmcd->pad) {
    Byte *padding;
    HBytes_Value nxthdr;

    rc= cht_pat_hb(ip,nxthdr_arg,&nxthdr);
    if (rc) return rc;

    if (cht_hb_len(&nxthdr) != 1) return
      cht_staticerr(ip, "RFC2406 next header field must be exactly 1 byte", 0);
    padlen= pmcd->blocksize-1 - ((cht_hb_len(pmcd->hb)+1) % pmcd->blocksize);
    padding= cht_hb_append(pmcd->hb, padlen+2);
    for (i=1; i<=padlen; i++)
      *padding++ = i;
    *padding++ = padlen;
    *padding++ = cht_hb_data(&nxthdr)[0];
    *ok= 1;
    
  } else {
    const Byte *padding, *trailer;
    HBytes_Value nxthdr;
    Tcl_Obj *nxthdr_valobj, *ro;
    
    *ok= 0;
    old_len= cht_hb_len(pmcd->hb);  if (old_len % pmcd->blocksize) goto quit;
    trailer= cht_hb_unappend(pmcd->hb, 2); if (!trailer) goto quit;

    padlen= trailer[0];
    cht_hb_array(&nxthdr,trailer+1,1);
    nxthdr_valobj= cht_ret_hb(ip,nxthdr);
    ro= Tcl_ObjSetVar2(ip,nxthdr_arg,0,nxthdr_valobj,TCL_LEAVE_ERR_MSG);
    if (!ro) { Tcl_DecrRefCount(nxthdr_valobj); return TCL_ERROR; }

    padding= cht_hb_unappend(pmcd->hb, padlen);
    for (i=1; i<=padlen; i++)
      if (*padding++ != i) goto quit;

    *ok= 1;

  quit:;

  }

  return TCL_OK;
}

int cht_do_padmethodinfo_pkcs5(ClientData cd, Tcl_Interp *ip, int *ok) {
  const PadMethodClientData *pmcd= (const void*)cd;
  int padlen, old_len, i;
  
  if (pmcd->blocksize > 255)
    return cht_staticerr(ip, "block size too large for pkcs#5", 0);

  if (pmcd->pad) {

    Byte *padding;

    padlen= pmcd->blocksize - (cht_hb_len(pmcd->hb) % pmcd->blocksize);
    padding= cht_hb_append(pmcd->hb, padlen);
    memset(padding, padlen, padlen);

  } else {

    const Byte *padding;

    old_len= cht_hb_len(pmcd->hb);  if (old_len % pmcd->blocksize) goto bad;
    padding= cht_hb_unappend(pmcd->hb, 1);  if (!padding) goto bad;
    padlen= *padding;
    if (padlen < 1 || padlen > pmcd->blocksize) goto bad;
    padding= cht_hb_unappend(pmcd->hb, padlen-1);  if (!padding) goto bad;

    for (i=0; i<padlen-1; i++, padding++) if (*padding != padlen) goto bad;

  }

  *ok= 1;
  return TCL_OK;

 bad:
  *ok= 0;
  return TCL_OK;
}

#define OBJ_CIPHKEY(o) ((CiphKeyValue*)(o)->internalRep.otherValuePtr)

typedef struct {
  int valuelen, bufferslen;
  Byte *value, *buffers;
  const void *alg;
  void *alpha, *beta; /* key schedules etc.; each may be 0 */
} CiphKeyValue;

static void freealg(CiphKeyValue *key) {
  TFREE(key->alpha);
  TFREE(key->beta);
}

static void key_t_free(Tcl_Obj *obj) {
  CiphKeyValue *key= OBJ_CIPHKEY(obj);
  freealg(key);
  TFREE(key->value);
  TFREE(key->buffers);
}

static void noalg(CiphKeyValue *key) {
  key->alg= 0;
  key->alpha= key->beta= 0;
}

static void key_t_dup(Tcl_Obj *src_obj, Tcl_Obj *dup_obj) {
  CiphKeyValue *src= OBJ_CIPHKEY(src_obj);
  CiphKeyValue *dup= TALLOC(sizeof(*dup));
  dup->valuelen= src->valuelen;
  dup->value= src->valuelen ? TALLOC(src->valuelen) : 0;
  dup->buffers= 0; dup->bufferslen= 0;
  memcpy(dup->value, src->value, src->valuelen);
  noalg(dup);
  dup_obj->internalRep.otherValuePtr= dup;
  dup_obj->typePtr= &cht_blockcipherkey_type;
}

static void key_t_ustr(Tcl_Obj *o) {
  cht_obj_updatestr_array(o, OBJ_CIPHKEY(o)->value, OBJ_CIPHKEY(o)->valuelen);
}

static int key_t_sfa(Tcl_Interp *ip, Tcl_Obj *o) {
  int rc, l;
  CiphKeyValue *val;

  rc= Tcl_ConvertToType(ip,o,&cht_hbytes_type);  if (rc) return rc;
  val= TALLOC(sizeof(*val));
  val->valuelen= l= cht_hb_len(OBJ_HBYTES(o));
  val->value= TALLOC(l);
  val->buffers= 0;
  val->bufferslen= 0;
  memcpy(val->value, cht_hb_data(OBJ_HBYTES(o)), l);
  noalg(val);

  cht_objfreeir(o);
  o->internalRep.otherValuePtr= val;
  o->typePtr= &cht_blockcipherkey_type;

  return TCL_OK;
}
  
Tcl_ObjType cht_blockcipherkey_type = {
  "blockcipher-key",
  key_t_free, key_t_dup, key_t_ustr, key_t_sfa
};

static CiphKeyValue *get_key(Tcl_Interp *ip, Tcl_Obj *key_obj,
			     const void *alg, int want_bufferslen) {
  CiphKeyValue *key;
  int rc;
  
  rc= Tcl_ConvertToType(ip,key_obj,&cht_blockcipherkey_type);  if (rc) return 0;
  key= OBJ_CIPHKEY(key_obj);

  if (key->alg != alg) {
    freealg(key);
    noalg(key);
    key->alg= alg;
  }
  
  if (key->bufferslen < want_bufferslen) {
    TFREE(key->buffers);
    key->buffers= TALLOC(want_bufferslen);
    key->bufferslen= want_bufferslen;
  }
  return key;
}

int cht_do_hbcrypto_blockcipher(ClientData cd, Tcl_Interp *ip,
			  const BlockCipherOp *op,
			  int objc, Tcl_Obj *const *objv) {
  return op->func((void*)op,ip,objc,objv);
}

static int blockcipher_prep(Tcl_Interp *ip, Tcl_Obj *key_obj,
			    const HBytes_Value *iv, int decrypt,
			    const BlockCipherAlgInfo *alg,
			    const BlockCipherModeInfo *mode, int data_len,
			    const CiphKeyValue **key_r, const void **sched_r,
			    const Byte **iv_r, int *iv_lenbytes_r,
			    Byte **buffers_r, int *nblocks_r) {
  void *sched, **schedp;
  int want_bufferslen, want_iv;
  int rc;
  CiphKeyValue *key;

  /* placate gcc, see Debian #968734 */
  *key_r= 0;
  *sched_r= 0;
  *iv_r= 0;
  *iv_lenbytes_r= 0;
  *buffers_r= 0;
  *nblocks_r= 0;

  if (data_len % alg->blocksize)
    return cht_staticerr(ip, "block cipher input not whole number of blocks",
		     "HBYTES BLOCKCIPHER LENGTH");

  want_bufferslen= alg->blocksize * (mode->buf_blocks + mode->iv_blocks);
  key= get_key(ip, key_obj, alg, want_bufferslen);  if (!key) return TCL_ERROR;

  schedp= (alg->decrypt.make_schedule==alg->encrypt.make_schedule
	   || !decrypt) ? &key->alpha : &key->beta;
  sched= *schedp;
  if (!sched) {
    if (key->valuelen < alg->key_min)
      return cht_staticerr(ip, "key too short", "HBYTES BLOCKCIPHER PARAMS");
    if (key->valuelen > alg->key_max)
      return cht_staticerr(ip, "key too long", "HBYTES BLOCKCIPHER PARAMS");

    sched= TALLOC(alg->schedule_size);
    (decrypt ? &alg->decrypt : &alg->encrypt)->make_schedule
      (sched, key->value, key->valuelen);
    *schedp= sched;
  }

  want_iv= alg->blocksize * mode->iv_blocks;
  if (!want_iv) {
    if (!cht_hb_issentinel(iv))
      return cht_staticerr(ip,"iv supplied but mode does not take one", 0);
  } else if (cht_hb_issentinel(iv)) {
    if (decrypt) return cht_staticerr(ip,"must supply iv when decrypting", 0);
    rc= cht_get_urandom(ip, key->buffers, want_iv);
    if (rc) return rc;
  } else {
    int iv_supplied= cht_hb_len(iv);
    if (iv_supplied > want_iv)
      return cht_staticerr(ip, "iv too large for algorithm and mode",
		       "HBYTES BLOCKCIPHER PARAMS");
    memcpy(key->buffers, cht_hb_data(iv), iv_supplied);
    memset(key->buffers + iv_supplied, 0, want_iv - iv_supplied);
  }

  *key_r= key;
  *sched_r= sched;

  *iv_r= key->buffers;
  *iv_lenbytes_r= want_iv;

  *buffers_r= key->buffers + want_iv;
  *nblocks_r= data_len / alg->blocksize;
  
  return TCL_OK;
}

int cht_do_blockcipherop_d(ClientData cd, Tcl_Interp *ip,
		       HBytes_Var v, const BlockCipherAlgInfo *alg,
		       Tcl_Obj *key_obj, const BlockCipherModeInfo *mode,
		       HBytes_Value iv, HBytes_Value *result) {
  return cht_do_blockcipherop_e(cd,ip,v,alg,key_obj,mode,iv,result);
}

int cht_do_blockcipherop_e(ClientData cd, Tcl_Interp *ip,
		       HBytes_Var v, const BlockCipherAlgInfo *alg,
		       Tcl_Obj *key_obj, const BlockCipherModeInfo *mode,
		       HBytes_Value iv, HBytes_Value *result) {
  const BlockCipherOp *op= (const void*)cd;
  int encrypt= op->encrypt;
  int rc, iv_lenbytes;
  const CiphKeyValue *key;
  const char *failure;
  const Byte *ivbuf;
  Byte *buffers;
  const void *sched;
  int nblocks;

  if (!mode->encrypt)
    return cht_staticerr(ip, "mode does not support encrypt/decrypt", 0);

  rc= blockcipher_prep(ip,key_obj,&iv,!encrypt,
		       alg,mode, cht_hb_len(v.hb),
		       &key,&sched,
		       &ivbuf,&iv_lenbytes,
		       &buffers,&nblocks);
  if (rc) return rc;
  
  failure=
    (encrypt ? mode->encrypt : mode->decrypt)
    (cht_hb_data(v.hb), nblocks, ivbuf, buffers, alg, encrypt, sched);

  if (failure)
    return cht_staticerr(ip, failure, "HBYTES BLOCKCIPHER CRYPTFAIL CRYPT");

  cht_hb_array(result, ivbuf, iv_lenbytes);

  return TCL_OK;
}

int cht_do_blockcipherop_mac(ClientData cd, Tcl_Interp *ip,
			 HBytes_Value msg, const BlockCipherAlgInfo *alg,
			 Tcl_Obj *key_obj, const BlockCipherModeInfo *mode,
			 HBytes_Value iv, HBytes_Value *result) {
  const CiphKeyValue *key;
  const char *failure;
  const Byte *ivbuf;
  Byte *buffers;
  const void *sched;
  int nblocks, iv_lenbytes;
  int rc;

  if (!mode->mac)
    return cht_staticerr(ip, "mode does not support mac generation", 0);
  
  rc= blockcipher_prep(ip,key_obj,&iv,0,
		       alg,mode, cht_hb_len(&msg),
		       &key,&sched,
		       &ivbuf,&iv_lenbytes,
		       &buffers,&nblocks);
  if (rc) return rc;

  failure= mode->mac(cht_hb_data(&msg), nblocks, ivbuf, buffers, alg, sched);
  if (failure)
    return cht_staticerr(ip,failure, "HBYTES BLOCKCIPHER CRYPTFAIL MAC");

  cht_hb_array(result, buffers, alg->blocksize * mode->mac_blocks);

  return TCL_OK;
}

int cht_do_hbcrypto_hmac(ClientData cd, Tcl_Interp *ip, const HashAlgInfo *alg,
		   HBytes_Value message, Tcl_Obj *key_obj,
		   Tcl_Obj *maclen_obj, HBytes_Value *result) {
  /* key->alpha = state after H(K XOR ipad <unfinished>
   * key->beta = state after H(K XOR opad <unfinished>
   * key->buffers = room for one block, or one state
   */
  CiphKeyValue *key;
  Byte *dest;
  int i, ml, rc;

  if (maclen_obj) {
    rc= Tcl_GetIntFromObj(ip, maclen_obj, &ml);  if (rc) return rc;
    if (ml<0 || ml>alg->hashsize)
      return cht_staticerr(ip, "requested hmac output size out of range",
		       "HBYTES HMAC PARAMS");
  } else {
    ml= alg->hashsize;
  }

  key= get_key(ip, key_obj, alg,
	       alg->blocksize > alg->statesize
	       ? alg->blocksize : alg->statesize);

  if (!key->alpha) {
    assert(!key->beta);
    
    if (key->valuelen > alg->blocksize)
      return cht_staticerr(ip, "key to hmac longer than hash block size",
		       "HBYTES HMAC PARAMS");

    memcpy(key->buffers, key->value, key->valuelen);
    memset(key->buffers + key->valuelen, 0, alg->blocksize - key->valuelen);
    for (i=0; i<alg->blocksize; i++) key->buffers[i] ^= 0x36;

    key->alpha= TALLOC(alg->statesize);
    alg->init(key->alpha);
    alg->update(key->alpha, key->buffers, alg->blocksize);
    
    key->beta= TALLOC(alg->statesize);
    alg->init(key->beta);
    for (i=0; i<alg->blocksize; i++) key->buffers[i] ^= (0x5c ^ 0x36);
    alg->update(key->beta, key->buffers, alg->blocksize);
  }
  assert(key->beta);

  dest= cht_hb_arrayspace(result, alg->hashsize);

  memcpy(key->buffers, key->alpha, alg->statesize);
  alg->update(key->buffers, cht_hb_data(&message), cht_hb_len(&message));
  alg->final(key->buffers, dest);

  memcpy(key->buffers, key->beta, alg->statesize);
  alg->update(key->buffers, dest, alg->hashsize);
  alg->final(key->buffers, dest);

  cht_hb_unappend(result, alg->hashsize - ml);

  return TCL_OK;
}

int cht_do_blockcipherop_prop(ClientData cd, Tcl_Interp *ip,
			  const BlockCipherPropInfo *prop,
			  const BlockCipherAlgInfo *alg, int *result) {
  *result= *(const int*)((const char*)alg + prop->int_offset);
  return TCL_OK;
}

int cht_do_hbcrypto_hash_prop(ClientData cd, Tcl_Interp *ip,
			const HashAlgPropInfo *prop,
			const HashAlgInfo *alg, int *result) {
  *result= *(const int*)((const char*)alg + prop->int_offset);
  return TCL_OK;
}
